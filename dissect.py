#!/usr/bin/python3

import mdtraj
import sys
import argparse

ignored_residues = ['HOH', 'NA', 'CL', 'MN']

def pick_residues(topology, atoms):
    residues = set()
    for a in atoms:
        residues.add(topology.atom(a).residue)
    return residues

def summarize_bychain(residueset):
    ret = {}
    for r in residueset:
        if r.name in ignored_residues:
            continue
        chainid = r.chain.index
        if chainid not in ret:
            ret[chainid] = []
        ret[chainid].append((r.resSeq, r.name))
    for k in ret:
        ret[k].sort()
    for k in sorted(ret.keys()):
        print("Chain", chr(ord('A') + k))
        print(ret[k])

def main():
    parser = argparse.ArgumentParser(description="Dissect protein / nucleic acids by some distance")
    parser.add_argument("pdbfile", type=str, metavar='PDB',
                        help='Initial PDB file to select')
    parser.add_argument("selection", type=str, metavar='SELSTR',
                        help='Selection string (mdtraj format)')
    parser.add_argument("outputfile", type=str, metavar='PDB',
                        help='Output PDB file')
    parser.add_argument("--radius", default=20.0, type=float,
                        help='Radius of the cut (angstrom)')
    parser.add_argument("--boundary", default=2.0, type=float,
                        help='Atoms within boundary angstrom from the edge are considered boundary residues')
    parser.add_argument("--boundary-output", default="boundaries.txt", type=str,
                        help='Output boundary atom information to this file')
    parser.add_argument("--truncate-five-prime", dest="truncate_five_prime", default=True, action="store_true",
                        help='Truncate phosphate group at 5\'-end (default: on, AMBER-type)')
    parser.add_argument("--no-truncate-five-prime", dest="truncate_five_prime", default=True, action="store_false",
                        help='Do not truncate phosphate group at 5\'-end (CHARMM-type)')

    args = parser.parse_args()
    
    structure = mdtraj.load(args.pdbfile)
    topology = structure.topology

    selected = topology.select(args.selection)
    nearby = mdtraj.compute_neighbors(structure, args.radius * 0.1, selected, periodic=False)[0]
    inner = mdtraj.compute_neighbors(structure, (args.radius - args.boundary) * 0.1, selected, periodic=False)[0]

    boundary_atoms = list(set(nearby) - set(inner))
    boundary_atoms.sort()

    residues_boundary = pick_residues(topology, boundary_atoms)
    residues_inner = pick_residues(topology, inner)
    residues_inner -= residues_boundary

    print("Boundary residues:")
    summarize_bychain(residues_boundary)
    # summarize_bychain(residues_inner)
    selected_residues = residues_inner | residues_boundary

    # Prevent one-residue chain (this harms GROMACS)
    selected_residues_numbered = set([(r.chain.index, r.resSeq) for r in selected_residues])
    selected_residues_orphan_removed = set()
    for r in selected_residues:
        if (r.chain.index, r.resSeq - 1) in selected_residues_numbered or \
           (r.chain.index, r.resSeq + 1) in selected_residues_numbered:
            selected_residues_orphan_removed.add(r)
        else:
            print("Removed orphan residue %s" % str(r))
            if r in residues_boundary:
                residues_boundary.remove(r)
    selected_residues = selected_residues_orphan_removed
    selected_residues_numbered = set([(r.chain.index, r.resSeq) for r in selected_residues])

    selected_atoms = []
    for r in selected_residues:
        if r.name in ignored_residues:
            continue
        for a in r.atoms:
            if a.element == mdtraj.core.element.hydrogen:
                continue
            if (args.truncate_five_prime and
                (r.chain.index, r.resSeq - 1) not in selected_residues_numbered and 
                a.name in ['P', 'OP1', 'OP2']):
                continue
            selected_atoms.append(a.index)
    selected_atoms.sort()
    newstructure = structure.atom_slice(selected_atoms)
    bfactors = []
    # newstructure's topology have a copy of residue, which is imcompatible to selected_residues
    residues_boundary_trio = set()
    for r in residues_boundary:
        if r.name in ignored_residues:
            continue
        residues_boundary_trio.add((r.name, r.resSeq, r.chain.index))

    for a in newstructure.topology.atoms:
        r = a.residue
        bf = 0.0
        if (r.name, r.resSeq, r.chain.index) in residues_boundary_trio:
            bf = 1.0
        bfactors.append(bf)
    newstructure.save_pdb(args.outputfile, bfactors=bfactors)

    # OMFG mdtraj chain id is renumbereed upon save.
    chainids = set()
    for r in selected_residues:
        chainids.add(r.chain.index)
    chainids = sorted(list(chainids))
    chainid_renumber_map = { c: i for (i, c) in enumerate(chainids) }
        
    residues_boundary_trio_sorted = [(chain, resseq, name) for (name, resseq, chain) in residues_boundary_trio]
    residues_boundary_trio_sorted.sort()
    with open(args.boundary_output, "w") as fh:
        for (chain, resseq, name) in residues_boundary_trio_sorted:
            chainstr = chr(ord('A') + chainid_renumber_map[chain])
            print("%s\t%d\t%s" % (chainstr, resseq, name), file=fh)

if __name__ == '__main__':
    main()
