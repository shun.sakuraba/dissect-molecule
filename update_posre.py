
# update posre_Protein_chain*.itp and posre_DNA_chain*.itp based on boundaries.txt.

import glob
import sys
import re

[_, boundary] = sys.argv

boundaries = {}
with open(boundary) as fh:
    for l in fh:
        (chain, resid, resn) = l.split()
        resid = int(resid)
        boundaries[(chain, resid)] = resn

# Because we are going to rewrite files, glob may get confused. Thus we complete the glob operation here.
files = [(False, f) for f in glob.glob("topol_Protein_chain_*.itp")] + \
        [(True, f) for f in glob.glob("topol_DNA_chain_*.itp")]

sentinelstr = "; SENTINEL: must not execute twice"

for (is_DNA, f) in files:

    m = re.match(r"topol_(Protein|DNA)_chain_([A-Z])([0-9]*)\.itp", f)
    assert m is not None
    chain = m.group(2)

    # first read and determine which to restrain
    restrained_atoms = []

    with open(f) as fh:
        print("Reading %s:" % f)
        section = None
        for l in fh:
            assert l.strip() != sentinelstr, "Must not execute this program twice"
            if l.startswith("["):
                ls = l.split()
                section = ls[1]
                continue
            lreal = l.split(';', 1)[0]
            ls = lreal.split()
            if section == "atoms":
                if ls == []:
                    continue
                atomnr = int(ls[0])
                resnr = int(ls[2])
                resname = ls[3]
                atomname = ls[4]
                is_restr = True
                if (chain, resnr) in boundaries:
                    if not is_DNA:
                        assert resname == boundaries[(chain, resnr)] # no cys in the sequence, so seems to be valid...
                else:
                    is_restr = False
                if atomname.startswith("H"):
                    is_restr = False
                if is_DNA and is_restr:
                    if not ('P' in atomname or "'" in atomname):
                        # *hack*
                        # P, O1P, O2P, O3P, sugars
                        is_restr = False
                if is_restr:
                    restrained_atoms.append(atomnr)

    with open(f, 'a') as fh:
        print("Updating %s:" % f)
        print("#ifndef POSRES", file=fh)
        print(sentinelstr, file=fh)
        print("[ position_restraints ]", file=fh)
        for a in restrained_atoms:
            forceconst = 1000
            print("%d 1 %g %g %g" % (a, forceconst, forceconst, forceconst), file=fh)
        print("#endif", file=fh)
